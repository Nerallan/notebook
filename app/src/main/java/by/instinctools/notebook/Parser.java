package by.instinctools.notebook;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import by.instinctools.notebook.model.Note;
import by.instinctools.notebook.model.Response;
import by.instinctools.notebook.model.User;

/**
 * Created by Nerallan on 1/12/2019.
 */

public class Parser {
	private static final String TITLE = "title";
	private static final String DESCRIPTION = "description";
	private static final String IMAGE_URL = "image_url";
	private static final String ITEMS = "items";
	private static final String FAVORITES = "favorites";
	private static final String RATING = "rating";
	private static final String ITEM_ID = "item_id";
	private static final String TIMESTAMP = "timestamp";
	private static final String RATING_VALUE = "rate_value";


	private static final String USER_EMAIL = "email";
	private static final String USER_IMAGE = "user_image";
	private static final String USERNAME = "username";

	private static final String LOCAL_ID = "localId";
	private static final String ID_TOKEN = "idToken";
	private static final String REFRESH_TOKEN = "refreshToken";
	private static final String EXPIRES_IN = "expiresIn";

	private List<Note> noteList = null;
	


	@Nullable
	public List<Note> readData(InputStream in, String type) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

		switch (type){
			case ITEMS:
				noteList = readItems(reader);
				break;
			case FAVORITES:
				readFavorites(noteList, reader);
				break;
			case RATING:
				readRating(noteList, reader);
				break;
			default:
				reader.skipValue();
				break;
		}
		return noteList;
	}

	private void readRating(List<Note> noteList, JsonReader reader) throws IOException {
		System.out.println(reader.toString());
		reader.beginObject();
		while(reader.hasNext()){
			int id = Integer.parseInt(reader.nextName());
			reader.beginObject();
			while(reader.hasNext()){
				switch (reader.nextName()){
					case RATING_VALUE:
						noteList.get(id).setRating(reader.nextInt());
						break;
					default:
						reader.skipValue();
						break;
				}
			}
			reader.endObject();
		}
		reader.endObject();
	}

	@NonNull
	private List<Note> readItems(JsonReader reader) throws IOException{
		List<Note> noteList = new ArrayList<>();
		reader.beginArray();
		while (reader.hasNext()) {
			noteList.add(readNote(reader));
		}
		reader.endArray();
		return noteList;
	}


	@NonNull
	private void readFavorites(List<Note> noteList, JsonReader reader) throws IOException{
		String userId = PrefManager.getInstance(App.getInstance()).getUserId();

		reader.beginObject();
		while (reader.hasNext()){
			if (reader.nextName().equals(userId)){
				readListFavorites(noteList, reader);
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
	}

	@NonNull
	private List<Note> readListFavorites(List<Note> noteList, JsonReader reader) throws IOException{
		reader.beginObject();
		while(reader.hasNext()){
			int id = Integer.parseInt(reader.nextName());
			reader.beginObject();
			while(reader.hasNext()){
				switch (reader.nextName()){
					case TIMESTAMP:
						noteList.get(id).setTimestamp(reader.nextLong());
						noteList.get(id).setFavorite(true);
						break;
					default:
						reader.skipValue();
						break;
				}
			}
			reader.endObject();
		}
		reader.endObject();
		return noteList;
	}


	@NonNull
	private Note readNote(JsonReader reader) throws IOException {
		String title = null;
		String description = null;
		String imageUrl = null;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			switch (name) {
				case TITLE:
					title = reader.nextString();
					break;
				case DESCRIPTION:
					description = reader.nextString();
					break;
				case IMAGE_URL:
					imageUrl = reader.nextString();
					break;
				default:
					reader.skipValue();
					break;
			}
		}
		reader.endObject();
		return new Note(title, description, imageUrl);
	}

	public User readCurrentUser(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		String username = null;
		String email = null;
		String userImage = null;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			switch (name) {
				case USERNAME:
					username = reader.nextString();
					break;
				case USER_EMAIL:
					email = reader.nextString();
					break;
				case USER_IMAGE:
					userImage = reader.nextString();
					break;
				default:
					reader.skipValue();
					break;
			}
		}
		reader.endObject();
		return new User(username, email, userImage);
	}

	public Response readResponse(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		String localId = null;
		String idToken = null;
		String refreshToken = null;
		int expiresIn = 0;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			switch (name) {
				case LOCAL_ID:
					localId = reader.nextString();
					break;
				case ID_TOKEN:
					idToken = reader.nextString();
					break;
				case REFRESH_TOKEN:
					refreshToken = reader.nextString();
					break;
				case EXPIRES_IN:
					expiresIn = reader.nextInt();
					break;
				default:
					reader.skipValue();
					break;
			}
		}
		reader.endObject();
		return new Response(localId, idToken, refreshToken, expiresIn);
	}

	public Response readError(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

		reader.beginObject();
		Response response = null;
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("error")){
				response = readMessageError(reader);
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
		return response;
	}

	private Response readMessageError(JsonReader reader) throws IOException {
		reader.beginObject();
		String error = null;
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("message")){
				error = reader.nextString();
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
		return new Response(error);
	}
}
