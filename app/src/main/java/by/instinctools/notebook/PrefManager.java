package by.instinctools.notebook;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import by.instinctools.notebook.model.Response;
import by.instinctools.notebook.model.User;


public class PrefManager {

    private static final int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "by.instinctools.notebook.pref_manager";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String PREF_TOKEN = "by.instinctools.notebook.token_pref";
    public static final String PREFS_MEMORY = "by.instinctools.notebook.disk_memory_setting";
    private static final String MEMORY_SETTINGS = "memory_settings";

    private static final String LOCAL_ID = "localId";
    private static final String ID_TOKEN = "idToken";
    private static final String REFRESH_TOKEN = "refreshToken";
    private static final String EXPIRES_IN = "expiresIn";
    private static final String ERROR_MESSAGE = "error_message";

    private static final String FAVORITES = "favorites";
    private static final String RATING = "rating";

    private static final String USERNAME = "username";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_IMAGE = "user_image";

    @Nullable
    private static PrefManager prefManager = null;
    @NonNull
    private SharedPreferences pref;
    @NonNull
    private Editor editor;
    @NonNull
    private Context context;

    @NonNull
    public static PrefManager getInstance(Context context){
        if (prefManager == null) {
            prefManager = new PrefManager(context);
        }
        return prefManager;
    }

    private PrefManager(@NonNull Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }


    public void saveResponseInPref(Response response) {
        editor.putString(LOCAL_ID, response.getLocalId());
        editor.putString(ID_TOKEN, response.getIdToken());
        editor.putString(REFRESH_TOKEN, response.getRefreshToken());
        editor.putInt(EXPIRES_IN, response.getExpiresIn());
        editor.putString(ERROR_MESSAGE, response.getErrorMessage());
        editor.apply();
    }

    public void saveUserData(String username, String email, String userImage){
        editor.putString(USERNAME, username);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_IMAGE, userImage);
        editor.apply();
    }

    public User getUserData(){
        String username = pref.getString(USERNAME, null);
        String email = pref.getString(USER_EMAIL, null);
        String image = pref.getString(USER_IMAGE, null);
        return new User(username, email, image);
    }


    public void saveFavorites(String favorites){
        editor.putString(FAVORITES, favorites);
        editor.apply();
    }

    public String getFavorites(){
        return pref.getString(FAVORITES, null);
    }

    public void saveRating(String rating){
        editor.putString(RATING, rating);
        editor.apply();
    }

    public String getRating(){
        return pref.getString(RATING, null);
    }

    public String getErrorMessage(){
        return pref.getString(ERROR_MESSAGE, null);
    }

    public String getIdToken(){
        return pref.getString(ID_TOKEN, null);
    }

    public String getRefreshToken(){
        return pref.getString(REFRESH_TOKEN, null);
    }

    public String getUserId(){
        return pref.getString(LOCAL_ID, null);
    }

    public boolean getMemorySettings(){
        return pref.getBoolean(MEMORY_SETTINGS, true);
    }

    public void setMemorySettings(boolean isActive){
        editor.putBoolean(MEMORY_SETTINGS, isActive);
        editor.apply();
    }

    public void setIdToken(String tokenId){
        editor.putString(ID_TOKEN, tokenId);
        editor.apply();
    }

    public void removeTokenData(){
        editor.remove(LOCAL_ID);
        editor.remove(ID_TOKEN);
        editor.remove(REFRESH_TOKEN);
        editor.remove(EXPIRES_IN);
        editor.remove(ERROR_MESSAGE);
        editor.apply();
    }

    public void removeUserData(){
        editor.remove(USERNAME);
        editor.remove(USER_EMAIL);
        editor.remove(USER_IMAGE);
        editor.apply();
    }

    public void setCurrentTime(long currentTime){
        editor.putLong("current_time", currentTime);
        editor.apply();
    }

    public Long getCurrentTime(){
        return pref.getLong("current_time", 0);
    }

}
