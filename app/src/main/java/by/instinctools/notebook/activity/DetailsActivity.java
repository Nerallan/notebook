package by.instinctools.notebook.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import by.instinctools.notebook.App;
import by.instinctools.notebook.R;
import by.instinctools.notebook.util.ImageLoadManager;

import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.DESCRIPTION;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.IMAGE_URL;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.TITLE;
import static by.instinctools.notebook.util.NoteProvider.CONTENT_URI_ITEMS;

/**
 * Created by Nerallan on 1/8/2019.
 */

public class DetailsActivity extends AppCompatActivity  implements
		LoaderManager.LoaderCallbacks<Cursor>  {

	private static final String EXTRA_ROW_ID = "by.instinctools.notebook.image_url";
	private static final int DOWNLOAD_FROM_DB = 15;

	@Nullable
	private Toolbar topToolbar;
	@Nullable
	private Long rowId;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);

		topToolbar = findViewById(R.id.activity_detail_toolbar);
		topToolbar.setTitle("");
		setSupportActionBar(topToolbar);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		getIncomingIntent();
		getSupportLoaderManager().initLoader(DOWNLOAD_FROM_DB, null, this);
		Log.d("FINISH", "onCreate: " + System.currentTimeMillis());
	}


	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void getIncomingIntent() {
		rowId = getIntent().getLongExtra(EXTRA_ROW_ID, 0);
	}


	private void setDetails(String title, String description, String imagePath) {
		if (topToolbar != null) {
			topToolbar.setTitle(title);
		}
		TextView descriptionTextView = findViewById(R.id.description_detail_text_view);
		ImageView fruitImageView = findViewById(R.id.image_detail_view);
		if (description != null) {
			descriptionTextView.setText(description);
		}
		if (imagePath != null) {
			ImageLoadManager.getInstance().load(imagePath, fruitImageView, true);
		}
	}

	@NonNull
	public static Intent newIntent(@NonNull Context packageContext, @NonNull Long rowId) {
		Intent intent = new Intent(packageContext, DetailsActivity.class);
		intent.putExtra(EXTRA_ROW_ID, rowId);
		return intent;
	}

	@NonNull
	@Override
	public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
		String uri = CONTENT_URI_ITEMS + "/" + rowId;
		return new CursorLoader(App.getInstance(), Uri.parse(uri), null, null, null, null);
	}

	@Override
	public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor cursor) {
		if (cursor != null && cursor.moveToFirst()){
			cursor.moveToFirst();
			String title = cursor.getString(cursor.getColumnIndex(TITLE));
			String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION));
			String imagePath = cursor.getString(cursor.getColumnIndex(IMAGE_URL));
			setDetails(title, description, imagePath);
		}
		Log.d("FINISH", "onLoadFinished: " + System.currentTimeMillis());
	}

	@Override
	public void onLoaderReset(@NonNull Loader<Cursor> loader) {

	}
}
