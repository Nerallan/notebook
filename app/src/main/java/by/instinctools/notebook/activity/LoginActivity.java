package by.instinctools.notebook.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.R;
import by.instinctools.notebook.model.Response;
import by.instinctools.notebook.util.LoginLoader;

public class LoginActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Response>,  View.OnClickListener {

	private static final int DOWLOAD_TOKEN = 5;

	@Nullable
	private EditText emailEditText;
	@Nullable
	private EditText passwordEditText;
	@Nullable
	private Button loginButton;
	@Nullable
	private ProgressBar progressBar;
	@Nullable
	private CheckBox passwordCheckBox;
	@Nullable
	String userCredentials;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initValues();

		if (loginButton != null) {
			loginButton.setOnClickListener(this);
		}
		if (passwordCheckBox != null && passwordEditText != null){
			passwordCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked){
						passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
					} else {
						passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
					}
				}
			});
		}

		emailEditText.setText("user@mail.ru");
		passwordEditText.setText("123456");
	}

	private void initValues() {
		emailEditText = findViewById(R.id.email_edit_text);
		passwordEditText = findViewById(R.id.password_edit_text);
		loginButton = findViewById(R.id.login_button);
		progressBar = findViewById(R.id.progress_bar_login);
		passwordCheckBox = findViewById(R.id.checkbox_password);
	}

	private boolean isEmpty(@Nullable EditText editText) {
		CharSequence sequence = null;
		if (editText != null) {
			sequence = editText.getText().toString();
		}
		return TextUtils.isEmpty(sequence);
	}


	@Override
	public void onClick(View v) {
		if (emailEditText != null && isEmpty(emailEditText)) {
			emailEditText.setError(getText(R.string.required));
			Toast.makeText(getApplicationContext(), getText(R.string.required), Toast.LENGTH_SHORT)
				 .show();
		} else if (passwordEditText != null && isEmpty(passwordEditText)) {
			passwordEditText.setError(getText(R.string.required));
			Toast.makeText(getApplicationContext(), getText(R.string.required), Toast.LENGTH_SHORT)
				 .show();
		} else {
			if (emailEditText != null && passwordEditText != null && loginButton != null)  {
				loginButton.setEnabled(false);
				if (progressBar != null) {
					progressBar.setVisibility(View.VISIBLE);
				}
				userCredentials = emailEditText.getText() + ":" + passwordEditText.getText();

				getSupportLoaderManager().restartLoader(DOWLOAD_TOKEN, null, this);
			}
		}
	}


	@Override
	protected void onStart() {
		super.onStart();
	}

	private void updateUI() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}

	@NonNull
	@Override
	public Loader<Response> onCreateLoader(int id, @Nullable Bundle bundle) {
		return new LoginLoader(LoginActivity.this, userCredentials);
	}

	@Override
	public void onLoadFinished(@NonNull Loader<Response> loader, Response response) {
		if (loginButton != null) {
			loginButton.setEnabled(true);
		}
		if (progressBar != null) {
			progressBar.setVisibility(View.GONE);
		}
		if (response.getIdToken() != null && response.getErrorMessage() == null){
			long currentTime = System.currentTimeMillis();
			PrefManager.getInstance(this).setCurrentTime(currentTime);
			updateUI();
		} else {
			Toast.makeText(this, response.getErrorMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onLoaderReset(@NonNull Loader<Response> loader) {
	}
}
