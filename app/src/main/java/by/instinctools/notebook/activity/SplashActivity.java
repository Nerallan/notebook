package by.instinctools.notebook.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.util.IntroLoader;

public class SplashActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Class<?>> {

	private static final int DELAY = 1000;
	private static final int GET_TARGER_SCREEN = 7;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportLoaderManager().initLoader(GET_TARGER_SCREEN, null, this);
	}


	private void launchScreen(final Class targetActivity) {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(getApplicationContext(), targetActivity);
				startActivity(intent);
				finish();
			}
		}, DELAY);
	}

	@NonNull
	@Override
	public Loader<Class<?>> onCreateLoader(int i, @Nullable Bundle bundle) {
		return new IntroLoader(SplashActivity.this);
	}

	@Override
	public void onLoadFinished(@NonNull Loader<Class<?>> loader, Class<?> aClass) {
		launchScreen(aClass);
	}

	@Override
	public void onLoaderReset(@NonNull Loader<Class<?>> loader) {
	}
}
