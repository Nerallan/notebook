package by.instinctools.notebook.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.R;
import by.instinctools.notebook.fragment.IntroFragments;

public class IntroActivity extends AppCompatActivity {

	private static final int NUM_ITEMS = 3;
	@Nullable
	private ViewPager viewPager;
	@Nullable
	private int[] layouts;
	@Nullable
	private Button buttonSkip;
	@Nullable
	private Button buttonNext;
	@Nullable
	private PrefManager prefManager;
	@Nullable
	private TabLayout tabLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		prefManager = PrefManager.getInstance(this);

		if (Build.VERSION.SDK_INT >= 21) {
			getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
		}
		setContentView(R.layout.activity_intro);
		initViews();

		changeStatusBarColor();

		ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
		if (viewPager != null) {
			viewPager.setAdapter(viewPagerAdapter);
			viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
		}


		if (tabLayout != null) {
			tabLayout.setupWithViewPager(viewPager, true);
		}

		if (buttonSkip != null) {
			buttonSkip.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					launchLoginScreen();
				}
			});
		}

		if (buttonNext != null && layouts != null) {
			buttonNext.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					int current = getItem(+ 1);
					if (current < layouts.length) {
						viewPager.setCurrentItem(current);
					} else {
						launchLoginScreen();
					}
				}
			});
		}
	}

	private void initViews() {
		viewPager = findViewById(R.id.view_pager);

		tabLayout = findViewById(R.id.tab_layout);

		buttonSkip = findViewById(R.id.btn_skip);
		buttonNext = findViewById(R.id.btn_next);

		layouts = new int[]{R.layout.intro_slide_first, R.layout.intro_slide_second,
				R.layout.intro_slide_third
		};
	}

	private int getItem(int i) {
		return (viewPager != null ? viewPager.getCurrentItem() : 0) + i;
	}

	private void launchLoginScreen() {
		if (prefManager != null) {
			prefManager.setFirstTimeLaunch(false);
		}
		startActivity(new Intent(IntroActivity.this, LoginActivity.class));
		finish();
	}


	ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

		@Override
		public void onPageSelected(int position) {
			if (position == layouts.length - 1) {
				buttonNext.setText(getString(R.string.start));
				buttonSkip.setVisibility(View.GONE);
			} else {
				buttonNext.setText(getString(R.string.next));
				buttonSkip.setVisibility(View.VISIBLE);
			}
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}
	};

	private void changeStatusBarColor() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(Color.TRANSPARENT);
		}
	}


	public class ViewPagerAdapter extends FragmentStatePagerAdapter {
		ViewPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return IntroFragments.newInstance(position);
		}

		@Override
		public int getCount() {
			return NUM_ITEMS;
		}
	}
}
