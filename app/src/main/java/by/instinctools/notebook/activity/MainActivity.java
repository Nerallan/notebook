package by.instinctools.notebook.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amitshekhar.DebugDB;

import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.R;
import by.instinctools.notebook.fragment.ProfileFragment;
import by.instinctools.notebook.fragment.SettingsFragment;
import by.instinctools.notebook.fragment.TabsFragment;
import by.instinctools.notebook.model.User;
import by.instinctools.notebook.util.ImageLoadManager;
import by.instinctools.notebook.util.SharedPrefLoader;

/**
 * Created by Nerallan on 1/6/2019.
 */

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<User>,
		NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

	private static final String TAG = "MainActivity";
	private static final int DOWLOAD_USER = 2;


	@Nullable
	private DrawerLayout drawerLayout;
	@Nullable
	private NavigationView navigationView;
	@Nullable
	private String id;

	@Nullable
	FragmentManager manager;
	@Nullable
	FragmentTransaction transaction;
	@Nullable
	ProfileFragment profileFragment;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_navigation_drawer);
		Toolbar topToolbar = findViewById(R.id.activity_main_toolbar);

		setSupportActionBar(topToolbar);

		initViews();

		System.out.println(DebugDB.getAddressLog());

		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, topToolbar,
				R.string.navigation_drawer_open, R.string.navigation_drawer_close);
		if (drawerLayout != null) {
			drawerLayout.addDrawerListener(toggle);
		}
		toggle.syncState();

		if (navigationView != null) {
			navigationView.setNavigationItemSelectedListener(this);
			navigationView.getMenu().getItem(0).setChecked(true);
		}

		id = PrefManager.getInstance(this).getUserId();
		if (id != null) {
			getSupportLoaderManager().initLoader(DOWLOAD_USER, null, this);
		}

		manager = getSupportFragmentManager();

		if (manager != null) {
			transaction = manager.beginTransaction();
		}
		if (transaction != null) {
			transaction.replace(R.id.fragment_container, TabsFragment.newInstance());
			transaction.commit();
		}
	}


	private void initViews() {
		drawerLayout = findViewById(R.id.drawer_layout);
		navigationView = findViewById(R.id.nav_view);
	}


	@Override
	public boolean onPrepareOptionsMenu(@NonNull Menu menu) {
		if (PrefManager.getInstance(this).getMemorySettings()) {
			menu.findItem(R.id.turn_memory_check_box).setChecked(true);
		} else {
			menu.findItem(R.id.turn_memory_check_box).setChecked(false);
		}
		return super.onPrepareOptionsMenu(menu);
	}


	@Override
	public boolean onCreateOptionsMenu(@NonNull Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		switch (item.getItemId()) {
			case R.id.turn_memory_check_box:
				if (item.isChecked()) {
					item.setChecked(false);
					Toast.makeText(getApplication(), R.string.sd_card_not_active,
							Toast.LENGTH_LONG).show();
					PrefManager.getInstance(this).setMemorySettings(false);
				} else {
					item.setChecked(true);
					Toast.makeText(getApplication(), R.string.sd_card_active, Toast.LENGTH_LONG)
						 .show();
					PrefManager.getInstance(this).setMemorySettings(true);
				}
				return true;
			case android.R.id.home:
				if (drawerLayout != null && navigationView != null) {
					drawerLayout.openDrawer(GravityCompat.START);
				}
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
		int id = menuItem.getItemId();
		if (id == R.id.nav_home) {
			if (navigationView != null) {
				navigationView.getMenu().getItem(0).setChecked(true);
				navigationView.getMenu().getItem(1).setChecked(false);
			}
			if (manager != null) {
				transaction = manager.beginTransaction();
			}
			if (transaction != null) {
				transaction.replace(R.id.fragment_container, TabsFragment.newInstance());
				transaction.commit();
			}
		} else if (id == R.id.nav_logout) {
			PrefManager.getInstance(this).removeTokenData();
			PrefManager.getInstance(this).removeUserData();
			Intent loginIntent = new Intent(this, LoginActivity.class);
			startActivity(loginIntent);
			finish();
		} else if(id == R.id.settings){
			if (navigationView != null) {
				navigationView.getMenu().getItem(1).setChecked(true);
				navigationView.getMenu().getItem(0).setChecked(false);
			}
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.fragment_container, new SettingsFragment())
					.commit();
		}
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		return true;
	}


	@Override
	public void onBackPressed() {
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		} else {
			super.onBackPressed();
		}
	}


	public void updateNavHeader(@NonNull String username, @NonNull String email, @NonNull String userImage) {
		navigationView = findViewById(R.id.nav_view);
		View headerView = navigationView.getHeaderView(0);
		TextView navUsername = headerView.findViewById(R.id.username_navdrawer);
		TextView navEmail = headerView.findViewById(R.id.email_text_navdrawer);
		ImageView navImage = headerView.findViewById(R.id.image_view_navdrawer);

		navImage.setOnClickListener(this);
		if (PrefManager.getInstance(this).getIdToken() != null){
			navUsername.setText(username);
			navEmail.setText(email);
			ImageLoadManager.getInstance().load(userImage, navImage, false);
		}
	}

	@Override
	public void onClick(View v) {
		profileFragment = ProfileFragment.newInstance(id);
		manager = getSupportFragmentManager();
		transaction = manager.beginTransaction();
		if (profileFragment != null) {
			transaction.replace(R.id.fragment_container, profileFragment);
		}
		if (navigationView != null) {
			navigationView.getMenu().getItem(0).setChecked(false);
			navigationView.getMenu().getItem(1).setChecked(false);
		}
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		drawer.closeDrawer(GravityCompat.START);
		transaction.commit();
	}

	@NonNull
	@Override
	public Loader<User> onCreateLoader(int i, @Nullable Bundle bundle) {
		return new SharedPrefLoader(MainActivity.this);
	}

	@Override
	public void onLoadFinished(@NonNull Loader<User> loader, @Nullable User user) {
		if (user != null) {
			String username = user.getUsername();
			String email = user.getEmail();
			String image = user.getUserImage();
			if (username != null && email != null && image != null){
				updateNavHeader(username, email, image);
			}
		}
	}

	@Override
	public void onLoaderReset(@NonNull Loader<User> loader) {
	}
}