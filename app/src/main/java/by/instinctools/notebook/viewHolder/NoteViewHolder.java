package by.instinctools.notebook.viewHolder;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import by.instinctools.notebook.App;
import by.instinctools.notebook.R;
import by.instinctools.notebook.fragment.DialogFragment;
import by.instinctools.notebook.util.FavoriteIntentService;
import by.instinctools.notebook.util.ImageLoadManager;

import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.DESCRIPTION;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.IMAGE_URL;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.TITLE;

/**
 * Created by Nerallan on 1/8/2019.
 */
public class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
		DialogFragment.Rating {
	@Nullable
	private TextView titleTextView;
	@Nullable
	private TextView descriptionTextView;
	@Nullable
	private ImageView fruitImageView;
	@Nullable
	private ImageView favoriteImageView;
	@Nullable
	private TextView ratingTextView;
	@Nullable
	private ListenerPosition listenerPosition;


	public NoteViewHolder(final View view, @Nullable final ListenerPosition listenerPosition) {
		super(view);
		this.titleTextView = view.findViewById(R.id.title_text_view);
		this.descriptionTextView = view.findViewById(R.id.description_text_view);
		this.fruitImageView = view.findViewById(R.id.fruit_image_view);
		this.favoriteImageView = view.findViewById(R.id.favorite_image_view);
		this.ratingTextView = view.findViewById(R.id.ratingBar_small);
		this.listenerPosition = listenerPosition;

		if (ratingTextView != null) {
			ratingTextView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Bundle args = new Bundle();
					args.putString("rating", ratingTextView.getText().toString());
					DialogFragment dialogFragment = new DialogFragment();
					dialogFragment.setArguments(args);
					FragmentManager manager = ((Activity) view.getContext()).getFragmentManager();
					dialogFragment.show(manager, "DialogFragment");
				}
			});
		}

		if (favoriteImageView != null) {
			favoriteImageView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Long time = System.currentTimeMillis();
					Intent intent = new Intent(view.getContext(), FavoriteIntentService.class);
					intent.putExtra("position", getAdapterPosition());
					intent.putExtra("timestamp", time);

					if (favoriteImageView.getColorFilter() != null){
						intent.putExtra("operationType", "delete");
						view.getContext().startService(intent);
						favoriteImageView.clearColorFilter();
					} else {
						intent.putExtra("operationType", "put");
						view.getContext().startService(intent);
						favoriteImageView.setColorFilter(ContextCompat.getColor(view.getContext(), R.color.colorAccent));
					}
				}
			});
		}
		itemView.setOnClickListener(this);
	}

	public void bindCursor(@Nullable Cursor cursor) {
		if (cursor != null){
			String image_path = cursor.getString(cursor.getColumnIndex(IMAGE_URL));
			if (fruitImageView != null) {
				ImageLoadManager.getInstance().load(image_path, fruitImageView, false);
			}
			if (titleTextView != null) {
				String title = cursor.getString(cursor.getColumnIndex(TITLE));
				titleTextView.setText(title);
			}
			if (descriptionTextView != null) {
				String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION));
				if (URLUtil.isHttpUrl(description) || URLUtil.isHttpsUrl(description)) {
					descriptionTextView.setText("");
				} else {
					descriptionTextView.setText(description);
				}
			}
		}
	}

	@Override
	public void onClick(View view) {
		if (listenerPosition != null) {
			listenerPosition.position(getAdapterPosition() - 1);
		}
	}

	@Override
	public void sendRating(int value) {
		if (ratingTextView != null) {
			ratingTextView.setText(value);
		}
	}


	public interface ListenerPosition {
		void position(int pos);
	}
}
