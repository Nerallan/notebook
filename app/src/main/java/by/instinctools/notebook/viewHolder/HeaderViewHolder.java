package by.instinctools.notebook.viewHolder;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import by.instinctools.notebook.R;

/**
 * Created by Nerallan on 1/8/2019.
 */
public class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	@Nullable
	private TextView headerTitle;

	public HeaderViewHolder(View view) {
		super(view);
		headerTitle = view.findViewById(R.id.header_text);
		headerTitle.setOnClickListener(this);
	}

	public void bindHeader() {
		if (headerTitle != null) {
			headerTitle.setText(R.string.header_text);
		}
	}

	@Override
	public void onClick(View v) {
		Toast.makeText(itemView.getContext(), R.string.toast_header_click, Toast.LENGTH_SHORT)
			 .show();
	}
}
