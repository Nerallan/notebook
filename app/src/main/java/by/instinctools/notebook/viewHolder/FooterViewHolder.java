package by.instinctools.notebook.viewHolder;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import by.instinctools.notebook.R;

/**
 * Created by Nerallan on 1/8/2019.
 */
public class FooterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	@Nullable
	private TextView footerText;

	public FooterViewHolder(View view) {
		super(view);
		footerText = view.findViewById(R.id.footer_text);
		footerText.setOnClickListener(this);
	}

	public void bindFooter() {
		if (footerText != null) {
			footerText.setText(R.string.footer_text);
		}
	}

	@Override
	public void onClick(View v) {
		Toast.makeText(itemView.getContext(), R.string.toast_footer_click, Toast.LENGTH_SHORT)
			 .show();
	}
}
