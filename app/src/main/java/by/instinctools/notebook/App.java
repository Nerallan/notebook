package by.instinctools.notebook;

import android.app.Application;
import android.content.Context;

/**
 * Created by Nerallan on 1/23/2019.
 */

public class App extends Application {

	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		App.context = getApplicationContext();
	}

	public static Context getInstance() {
		return App.context;
	}
}
