package by.instinctools.notebook.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import by.instinctools.notebook.App;
import by.instinctools.notebook.R;
import by.instinctools.notebook.fragment.TestFragment;
import by.instinctools.notebook.model.FragmentType;

public class ViewPagerAdapter extends FragmentStatePagerAdapter{

	private static final int FRAGMENT_NUM = 3;

	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		Fragment fragment = null;
		switch (position){
			case 0:
				fragment = TestFragment.newInstance(FragmentType.ALL);
				break;
			case 1:
				fragment = TestFragment.newInstance(FragmentType.RATING);
				break;
			case 2:
				fragment = TestFragment.newInstance(FragmentType.FAVORITES);
				break;
			default:
				break;
		}
		return fragment;
	}

	@Nullable
	@Override
	public CharSequence getPageTitle(int position) {
		String title = null;
		switch (position){
			case 0:
				title = App.getInstance().getString(R.string.default_list);
				break;
			case 1:
				title =  App.getInstance().getString(R.string.rated_list);
				break;
			case 2:
				title =  App.getInstance().getString(R.string.created_list);
				break;
			default:
				break;
		}
		return title;
	}

	@Override
	public int getCount() {
		return FRAGMENT_NUM;
	}
}
