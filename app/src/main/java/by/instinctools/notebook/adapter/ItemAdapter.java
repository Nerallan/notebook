package by.instinctools.notebook.adapter;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import by.instinctools.notebook.R;
import by.instinctools.notebook.viewHolder.FooterViewHolder;
import by.instinctools.notebook.viewHolder.HeaderViewHolder;
import by.instinctools.notebook.viewHolder.NoteViewHolder;

import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.UUID;

/**
 * Created by Nerallan on 1/5/2019.
 */

public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements NoteViewHolder.ListenerPosition {

	private static final int TYPE_HEADER = 0;
	private static final int TYPE_FOOTER = 1;
	private static final int TYPE_ITEM = 2;

	@NonNull
	private ListenerCursor listenerExternalId;
	@NonNull
	private Cursor cursor;


	public ItemAdapter(@NonNull Cursor cursor, @NonNull ListenerCursor listenerExternalId){
		this.cursor = cursor;
		this.listenerExternalId = listenerExternalId;
		swapCursor(cursor);
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
		if (viewType == TYPE_HEADER) {
			View view = LayoutInflater.from(viewGroup.getContext())
									  .inflate(R.layout.layout_header, viewGroup, false);
			return new HeaderViewHolder(view);
		} else if (viewType == TYPE_FOOTER) {
			View view = LayoutInflater.from(viewGroup.getContext())
									  .inflate(R.layout.layout_footer, viewGroup, false);
			return new FooterViewHolder(view);
		}
		View view = LayoutInflater.from(viewGroup.getContext())
								  .inflate(R.layout.list_row, viewGroup, false);
		return new NoteViewHolder(view, this);
	}


	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
		if (position == 0) {
			HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
			headerViewHolder.bindHeader();
		} else if (position == cursor.getCount() + 1) {
			FooterViewHolder footerHolder = (FooterViewHolder) viewHolder;
			footerHolder.bindFooter();
		} else {
			NoteViewHolder noteViewHolder = (NoteViewHolder) viewHolder;
			cursor.moveToPosition(noteViewHolder.getAdapterPosition() - 1);
			noteViewHolder.bindCursor(cursor);
		}
	}


	private void swapCursor(Cursor cursor) {
		this.cursor = cursor;
		notifyDataSetChanged();
	}


	@Override
	public int getItemCount() {
		return cursor.getCount() + 2;
	}


	@Override
	public int getItemViewType(int position) {
		if (position == 0) {
			return TYPE_HEADER;
		} else if (position == cursor.getCount() + 1) {
			return TYPE_FOOTER;
		}
		return TYPE_ITEM;
	}


	@Override
	public void position(int pos) {
		getCursorByIndex(pos);
	}


	private void getCursorByIndex(int pos) {
		cursor.moveToPosition(pos);
		Long id = cursor.getLong(cursor.getColumnIndex(UUID));
		listenerExternalId.getExternalId(id);
	}


	public interface ListenerCursor {
		void getExternalId(@NonNull Long id);
	}
}
