package by.instinctools.notebook.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ProgressBar;
import android.widget.Toast;

import by.instinctools.notebook.App;
import by.instinctools.notebook.NoteItemDecoration;
import by.instinctools.notebook.R;
import by.instinctools.notebook.activity.DetailsActivity;
import by.instinctools.notebook.adapter.ItemAdapter;
import by.instinctools.notebook.util.DownloadIntentService;

import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.DESCRIPTION;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.IMAGE_URL;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.TITLE;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.Cols.UUID;
import static by.instinctools.notebook.util.NoteProvider.CONTENT_URI_ITEMS;

public class RecyclerMainFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final int DOWNLOAD_FROM_DB = 1;

	@Nullable
	private RecyclerView recyclerView;
	@Nullable
	private ProgressBar progressBar;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_recycler_main, container, false);
		initViews(view);

		if (recyclerView != null) {
			recyclerView.setHasFixedSize(true);
			recyclerView.setLayoutManager(new LinearLayoutManager(App.getInstance()));
			NoteItemDecoration noteItemDecoration = new NoteItemDecoration(App.getInstance(),
					Color.GRAY, 1.5f);
			recyclerView.addItemDecoration(noteItemDecoration);
		}
		if (recyclerView != null) {
			recyclerView.setVisibility(View.GONE);
		}
		if (progressBar != null) {
			progressBar.setVisibility(View.VISIBLE);
		}
		if (getActivity() != null) {
			getActivity().getSupportLoaderManager().initLoader(DOWNLOAD_FROM_DB, null, this);
		}
		return view;
	}

	@NonNull
	@Override
	public Loader<Cursor> onCreateLoader(int i, @Nullable Bundle bundle) {
		return new CursorLoader(App.getInstance(), CONTENT_URI_ITEMS, null, null, null, null);
	}

	@Override
	public void onLoadFinished(@NonNull Loader<Cursor> loader, final Cursor cursor) {
		if (cursor.getCount() > 0) {
			ItemAdapter.ListenerCursor listenerCursor = new ItemAdapter.ListenerCursor() {
				@Override
				public void getExternalId(@NonNull Long id) {
					cursor.moveToPosition((int) (id - 1));
					String title = cursor.getString(cursor.getColumnIndex(TITLE));
					String description = cursor.getString(cursor.getColumnIndex(DESCRIPTION));
					String image_path = cursor.getString(cursor.getColumnIndex(IMAGE_URL));
					if (title != null && description != null && image_path != null) {
						if (URLUtil.isHttpUrl(description) || URLUtil.isHttpsUrl(description)) {
							openCustomChrome(description);
						} else if (getContext() != null) {
							Toast.makeText(getContext(), R.string.item_clicked + title,
									Toast.LENGTH_LONG).show();
							Intent intent = DetailsActivity.newIntent(getContext(),
									cursor.getLong(cursor.getColumnIndex(UUID)));
							startActivity(intent);
						}
					}
				}
			};
			if (recyclerView != null) {
				recyclerView.setAdapter(new ItemAdapter(cursor, listenerCursor));
				recyclerView.setVisibility(View.VISIBLE);
			}
			if (progressBar != null) {
				progressBar.setVisibility(View.GONE);
			}
		} else {
			Intent i = new Intent(getContext(), DownloadIntentService.class);
			App.getInstance().startService(i);
		}
	}

	@Override
	public void onLoaderReset(@NonNull Loader<Cursor> loader) {
	}


	public static RecyclerMainFragment newInstance() {
		return new RecyclerMainFragment();
	}


	private void initViews(View view) {
		progressBar = view.findViewById(R.id.progress_bar);
		recyclerView = view.findViewById(R.id.recycler_view_main);
	}

	private void openCustomChrome(@NonNull String description) {
		CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
		CustomTabsIntent customTabsIntent = builder.build();
		if (getContext() != null) {
			customTabsIntent.launchUrl(getContext(), Uri.parse(description));
		}
	}
}
