package by.instinctools.notebook.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import by.instinctools.notebook.R;

public class IntroFragments extends Fragment {

	static final String ARG_PAGE_NUMBER = "arg_page_number";

	int pageNumber;

	public static IntroFragments newInstance(int pos){
		IntroFragments introFragments = new IntroFragments();
		Bundle args = new Bundle();
		args.putInt(ARG_PAGE_NUMBER, pos);
		introFragments.setArguments(args);
		return introFragments;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		pageNumber = getArguments() != null ? getArguments().getInt(ARG_PAGE_NUMBER) : 1;
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = null;
		if (pageNumber == 0) {
			view = inflater.inflate(R.layout.intro_slide_first, container, false);
		} else if (pageNumber == 1) {
			view = inflater.inflate(R.layout.intro_slide_second, container, false);
		} else {
			view = inflater.inflate(R.layout.intro_slide_third, container, false);
		}
		return view;
	}
}
