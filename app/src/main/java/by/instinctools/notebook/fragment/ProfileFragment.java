package by.instinctools.notebook.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import by.instinctools.notebook.App;
import by.instinctools.notebook.R;
import by.instinctools.notebook.model.User;
import by.instinctools.notebook.util.ImageLoadManager;
import by.instinctools.notebook.util.SharedPrefLoader;

public class ProfileFragment extends Fragment implements
		LoaderManager.LoaderCallbacks<User>{

	private static final int GET_USER_FROM_PREF = 9;
	private static final String ARG_USER_ID = "user_id";

	@Nullable
	private ImageView profileImage;
	@Nullable
	private TextView usernameTextView;
	@Nullable
	private TextView emailTextView;
	@Nullable
	private String userId;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_profile, container, false);
		initViews(view);
		if (getArguments() != null){
			userId = getArguments().getString(ARG_USER_ID);
		}
		if (getActivity() != null) {
			getActivity().getSupportLoaderManager().initLoader(GET_USER_FROM_PREF, null, this);
		}

		return view;
	}

	private void initViews(View view) {
		profileImage = view.findViewById(R.id.image_view_profile);
		usernameTextView = view.findViewById(R.id.username_profile);
		emailTextView = view.findViewById(R.id.email_profile);
	}

	public static ProfileFragment newInstance(String id){
		ProfileFragment fragment = new ProfileFragment();
		Bundle args = new Bundle();
		args.putString(ARG_USER_ID, id);
		fragment.setArguments(args);
		return fragment;
	}


	@NonNull
	@Override
	public Loader<User> onCreateLoader(int i, @Nullable Bundle bundle) {
		return new SharedPrefLoader(App.getInstance());
	}

	@Override
	public void onLoadFinished(@NonNull Loader<User> loader, @Nullable User user) {
		if (user != null) {
			setUserData(user);
		}
	}

	private void setUserData(@NonNull User user) {
		if (usernameTextView != null) {
			usernameTextView.setText(user.getUsername());
		}
		if (emailTextView != null) {
			emailTextView.setText(user.getEmail());
		}
		if (profileImage != null && user.getUserImage() != null) {
			ImageLoadManager.getInstance().load(user.getUserImage(), profileImage, true);
		}
	}

	@Override
	public void onLoaderReset(@NonNull Loader<User> loader) {
	}
}
