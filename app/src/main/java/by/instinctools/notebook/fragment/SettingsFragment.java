package by.instinctools.notebook.fragment;

import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.PreferenceFragmentCompat;

import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.R;

public class SettingsFragment extends PreferenceFragmentCompat {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		EditTextPreference editTextFavoritePreference = (EditTextPreference) findPreference("favorites");
		String favoriteText = editTextFavoritePreference.getText();

		EditTextPreference editTextRatingPreference = (EditTextPreference) findPreference("rating");
		String ratingText = editTextFavoritePreference.getText();

		PrefManager.getInstance(getContext()).saveRating(ratingText);
		PrefManager.getInstance(getContext()).saveFavorites(favoriteText);
		//TODO check input value for digit and process with ok and cancel buttons
	}

	@Override
	public void onCreatePreferences(Bundle bundle, String rootKey) {
		setPreferencesFromResource(R.xml.settings, rootKey);
	}
}