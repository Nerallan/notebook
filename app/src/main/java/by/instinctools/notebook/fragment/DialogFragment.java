package by.instinctools.notebook.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import by.instinctools.notebook.App;
import by.instinctools.notebook.R;
import by.instinctools.notebook.model.Response;

public class DialogFragment extends android.app.DialogFragment implements LoaderManager.LoaderCallbacks<String>, View.OnClickListener {

	private static final String TAG = "DialogFragment";

	@Nullable
	private TextView okTextView;
	@Nullable
	private TextView cancelTextView;
	@Nullable
	private TextView ratingTextView;
	@Nullable
	private TextView increaseTextView;
	@Nullable
	private TextView dicreaseTextView;
	@Nullable
	public Rating ratingInterface;
	@Nullable
	private String rating;

	@NonNull
	@Override
	public Loader<String> onCreateLoader(int i, @Nullable Bundle bundle) {
		return null;
	}

	@Override
	public void onLoadFinished(@NonNull Loader<String> loader, String s) {

	}

	@Override
	public void onLoaderReset(@NonNull Loader<String> loader) {

	}

	public interface Rating {
		void sendRating(int value);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rating = getArguments().getString("rating");
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
							 Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_dialog, container, false);
		initViews(view);

		if (ratingTextView != null && rating != null){
			ratingTextView.setText(rating);
		}

		if (increaseTextView != null && dicreaseTextView != null && okTextView != null && cancelTextView != null) {
			increaseTextView.setOnClickListener(this);
			dicreaseTextView.setOnClickListener(this);
			okTextView.setOnClickListener(this);
			cancelTextView.setOnClickListener(this);
		}
		return view;
	}

	private void initViews(View view) {
		okTextView = view.findViewById(R.id.ok_text_view);
		cancelTextView = view.findViewById(R.id.cancel_text_view);
		ratingTextView = view.findViewById(R.id.rating_text_view);
		increaseTextView = view.findViewById(R.id.increase_rating_tv);
		dicreaseTextView = view.findViewById(R.id.dicrease_rating_tv);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.ok_text_view:
				Log.d(TAG, "onClick: closing dialog");
				if (ratingTextView != null && ratingInterface != null){
					ratingInterface.sendRating(Integer.parseInt(ratingTextView.getText().toString()));
				}
				getDialog().dismiss();
				break;
			case R.id.cancel_text_view:
				getDialog().dismiss();
				break;
			case R.id.increase_rating_tv:
				if (ratingTextView != null && increaseTextView != null && rating != null) {
					ratingTextView.setText(String.valueOf(Integer.parseInt(ratingTextView.getText().toString()) + 1));
					increaseTextView.setClickable(false);
				}
				break;
			case R.id.dicrease_rating_tv:
				if (ratingTextView != null && dicreaseTextView != null && rating != null) {
					ratingTextView.setText(String.valueOf(Integer.parseInt(ratingTextView.getText().toString()) - 1));
					dicreaseTextView.setClickable(false);
				}
				break;
		}
	}
}
