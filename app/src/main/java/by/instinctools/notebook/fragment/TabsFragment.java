package by.instinctools.notebook.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import by.instinctools.notebook.R;
import by.instinctools.notebook.adapter.ViewPagerAdapter;

public class TabsFragment extends Fragment {

	@Nullable
	private ViewPagerAdapter viewPagerAdapter;
	@Nullable
	private ViewPager viewPager;
	@Nullable
	private TabLayout tabLayout;


	public TabsFragment(){

	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
							 @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.tabs, container, false);

		initViews(view);

		viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
		if (viewPager != null && tabLayout != null) {
			viewPager.setAdapter(viewPagerAdapter);
			tabLayout.setupWithViewPager(viewPager);
		}
		return view;
	}

	private void initViews(View view) {
		viewPager = view.findViewById(R.id.view_pager_main);
		tabLayout = view.findViewById(R.id.tabs);
	}

	public static TabsFragment newInstance(){
		return new TabsFragment();
	}
}
