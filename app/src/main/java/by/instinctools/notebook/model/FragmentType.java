package by.instinctools.notebook.model;

import java.io.Serializable;

public enum FragmentType implements Serializable {
	ALL,
	RATING,
	FAVORITES
}
