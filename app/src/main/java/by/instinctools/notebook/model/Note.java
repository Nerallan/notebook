package by.instinctools.notebook.model;

import android.support.annotation.Nullable;

/**
 * Created by Nerallan on 1/5/2019.
 */

public class Note {
	@Nullable
	private String title;
	@Nullable
	private String description;
	@Nullable
	private String imagePath;
	private int rating;
	private boolean isFavorite;
	@Nullable
	private Long timestamp;


	public Note() {
	}

	public Note(@Nullable String title, @Nullable String description, @Nullable String imagePath) {
		this.title = title;
		this.description = description;
		this.imagePath = imagePath;
	}


	public Note(@Nullable String title, @Nullable String description, @Nullable String imagePath,
				int rating, boolean isFavorite, @Nullable Long timestamp) {
		this.title = title;
		this.description = description;
		this.imagePath = imagePath;
		this.rating = rating;
		this.isFavorite = isFavorite;
		this.timestamp = timestamp;
	}


	@Nullable
	public String getTitle() {
		return title;
	}

	public void setTitle(@Nullable String title) {
		this.title = title;
	}

	@Nullable
	public String getDescription() {
		return description;
	}

	public void setDescription(@Nullable String description) {
		this.description = description;
	}

	@Nullable
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(@Nullable String imagePath) {
		this.imagePath = imagePath;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public boolean isFavorite() {
		return isFavorite;
	}

	public void setFavorite(boolean favorite) {
		isFavorite = favorite;
	}

	@Nullable
	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(@Nullable Long timestamp) {
		this.timestamp = timestamp;
	}
}
