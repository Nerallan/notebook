package by.instinctools.notebook.model;

import android.support.annotation.Nullable;

public class Response {
	@Nullable
	private String localId;
	@Nullable
	private String idToken;
	@Nullable
	private String refreshToken;
	@Nullable
	private String errorMessage;
	private int expiresIn;

	public Response() {
	}

	public Response(@Nullable String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Response(@Nullable String localId, @Nullable String idToken,
					@Nullable String refreshToken, int expiresIn) {
		this.localId = localId;
		this.idToken = idToken;
		this.refreshToken = refreshToken;
		this.expiresIn = expiresIn;
	}

	@Nullable
	public String getLocalId() {
		return localId;
	}

	public void setLocalId(@Nullable String localId) {
		this.localId = localId;
	}

	@Nullable
	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(@Nullable String idToken) {
		this.idToken = idToken;
	}

	@Nullable
	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(@Nullable String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Nullable
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(@Nullable String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}
}
