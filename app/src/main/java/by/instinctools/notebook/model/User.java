package by.instinctools.notebook.model;

import android.support.annotation.Nullable;

public class User {

	@Nullable
	private String username;
	@Nullable
	private String email;
	@Nullable
	private String userImage;

	public User(){
	}

	public User(@Nullable String username, @Nullable String email, @Nullable String userImage){
		this.username = username;
		this.email = email;
		this.userImage = userImage;
	}


	@Nullable
	public String getUsername() {
		return username;
	}

	public void setUsername(@Nullable String username) {
		this.username = username;
	}

	@Nullable
	public String getEmail() {
		return email;
	}

	public void setEmail(@Nullable String email) {
		this.email = email;
	}

	@Nullable
	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(@Nullable String userImage) {
		this.userImage = userImage;
	}
}
