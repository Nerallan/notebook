package by.instinctools.notebook.database;

/**
 * Created by Nerallan on 1/27/2019.
 */

public class NoteDbSchema {

	public static final class NoteTable {
		public static final String NAME = "items";
		static final int DATABASE_VERSION = 1;

		public static final class Cols {
			public static final String UUID = "uuid";
			public static final String TITLE = "title";
			public static final String DESCRIPTION = "description";
			public static final String IMAGE_URL = "image_url";
		}
	}

	public static final class FavoriteTable{
		public static final String NAME = "favorites";
		static final int DATABASE_VERSION = 1;

		public static final class Cols {
			public static final String UUID = "uuid";
			public static final String ITEM_ID = "item_id";
			public static final String TIMESTAMP = "timestamp";
		}
	}

	public static final class RatingTable{
		public static final String NAME = "rating";
		static final int DATABASE_VERSION = 1;

		public static final class Cols {
			public static final String UUID = "uuid";
			public static final String ITEM_ID = "item_id";
			public static final String RATE_VALUE = "rate_value";
		}
	}
}
