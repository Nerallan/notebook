package by.instinctools.notebook.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import static by.instinctools.notebook.database.NoteDbSchema.*;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable.DATABASE_VERSION;

/**
 * Created by Nerallan on 1/27/2019.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "noteBase.db";

	private static final String db_items_create =  "create table if not exists " + NoteTable.NAME + "(" +
												NoteTable.Cols.UUID + ", " +
												NoteTable.Cols.TITLE + ", " +
												NoteTable.Cols.DESCRIPTION + ", "
												+ NoteTable.Cols.IMAGE_URL + ")";

	private static final String db_favorites_create =  "create table if not exists " + FavoriteTable.NAME + "(" +
			FavoriteTable.Cols.UUID + ", " +
			FavoriteTable.Cols.ITEM_ID + ", " +
			FavoriteTable.Cols.TIMESTAMP + ")";

	private static final String db_rating_create =  "create table if not exists " + RatingTable.NAME + "(" +
			RatingTable.Cols.UUID + ", " +
			RatingTable.Cols.ITEM_ID + ", " +
			RatingTable.Cols.RATE_VALUE + ")";


	private static final String db_items_drop = "DELETE FROM " + NoteTable.NAME;
	private static final String db_favirites_drop = "DELETE FROM " + FavoriteTable.NAME;
	private static final String db_rating_drop = "DELETE FROM " + RatingTable.NAME;

	public DatabaseHelper(@NonNull Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(@NonNull SQLiteDatabase db) {
		db.execSQL(db_items_create);
		db.execSQL(db_favorites_create);
		db.execSQL(db_rating_create);
		Log.d("TEST123", "DATABASE CREATED onCreate");
	}

	@Override
	public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("TEST123",
				"Upgrading by.instinctools.notebook.database from " + oldVersion + " to " + newVersion + " version");
		db.execSQL(db_items_drop);
		db.execSQL(db_favirites_drop);
		db.execSQL(db_rating_drop);
		onCreate(db);
	}
}
