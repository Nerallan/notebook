package by.instinctools.notebook.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.activity.IntroActivity;
import by.instinctools.notebook.activity.LoginActivity;
import by.instinctools.notebook.activity.MainActivity;

public class IntroLoader extends BaseLoader<Class<?>> {

	public IntroLoader(@NonNull Context context) {
		super(context);
	}

	@Nullable
	@Override
	public Class<?> loadInBackground() {
		PrefManager prefManager = PrefManager.getInstance(getContext());
		if (!prefManager.isFirstTimeLaunch()) {
			if (prefManager.getIdToken() != null) {
				return MainActivity.class;
			} else {
				return LoginActivity.class;
			}
		} else {
			return IntroActivity.class;
		}
	}
}
