package by.instinctools.notebook.util;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.Toast;

import by.instinctools.notebook.database.DatabaseHelper;

import static by.instinctools.notebook.database.NoteDbSchema.*;

/**
 * Created by Nerallan on 1/28/2019.
 */

public class NoteProvider extends ContentProvider{

	private static final String CONTENT_LIST = "vnd.android.cursor.dir/vnd.by.instonctools.androidcontentprovider.items";
	private static final String CONTENT_ITEM = "vnd.android.cursor.item/vnd.by.instonctools.androidcontentprovider.items";

	private static final String AUTHORITY = "by.instinctools.notebook.util.NoteProvider";

	private static final String ITEMS  = "/items";
	public static final Uri CONTENT_URI_ITEMS = Uri.parse("content://" + AUTHORITY + ITEMS);
	private static final String FAVORITES  = "/favorites";
	public static final Uri CONTENT_URI_FAVORITES = Uri.parse("content://" + AUTHORITY + FAVORITES);
	private static final String RATING  = "/rating";
	public static final Uri CONTENT_URI_RATING = Uri.parse("content://" + AUTHORITY + RATING);

	private static final int LIST_ITEMS = 1;
	private static final int PARTICULAR_ITEM = 2;
	private static final int LIST_FAVORITES = 3;
	private static final int PARTICULAR_FAVORITE = 4;
	private static final int LIST_RATING = 5;
	private static final int PARTICULAR_RATING = 6;

	private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	static {
		uriMatcher.addURI(AUTHORITY, ITEMS, LIST_ITEMS);
		uriMatcher.addURI(AUTHORITY, ITEMS + "/#", PARTICULAR_ITEM);
		uriMatcher.addURI(AUTHORITY, FAVORITES, LIST_FAVORITES);
		uriMatcher.addURI(AUTHORITY, FAVORITES + "/#", PARTICULAR_FAVORITE);
		uriMatcher.addURI(AUTHORITY, RATING, LIST_RATING);
		uriMatcher.addURI(AUTHORITY, RATING + "/#", PARTICULAR_RATING);
	}


	private static DatabaseHelper databaseHelper;

	@Override
	public boolean onCreate() {
		if (getContext() != null) {
			databaseHelper = new DatabaseHelper(getContext());
		}
		return false;
	}


	@Nullable
	@Override
	public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
						@Nullable String[] selectionArgs, @Nullable String sortOrder) {
		SQLiteDatabase database = databaseHelper.getWritableDatabase();
		Cursor cursor = null;

		switch (uriMatcher.match(uri)){
			case LIST_ITEMS:
				cursor = database.query(NoteTable.NAME, projection, selection, selectionArgs, null, null, null);
				break;
			case PARTICULAR_ITEM:
				selection = NoteTable.Cols.UUID + "=" + uri.getLastPathSegment();
				cursor = database.query(NoteTable.NAME, projection, selection, selectionArgs, null, null, null);
				break;
			case LIST_FAVORITES:
				cursor = database.query(FavoriteTable.NAME, projection, selection, selectionArgs, null, null, null);
				break;
			case PARTICULAR_FAVORITE:
				selection = FavoriteTable.Cols.UUID + "=" + uri.getLastPathSegment();
				cursor = database.query(FavoriteTable.NAME, projection, selection, selectionArgs, null, null, null);
				break;
			case LIST_RATING:
				cursor = database.query(RatingTable.NAME, projection, selection, selectionArgs, null, null, null);
				break;
			case PARTICULAR_RATING:
				selection = RatingTable.Cols.UUID + "=" + uri.getLastPathSegment();
				cursor = database.query(RatingTable.NAME, projection, selection, selectionArgs, null, null, null);
				break;
			default:
				Toast.makeText(getContext(), "Invalid content uri", Toast.LENGTH_LONG).show();
				throw new IllegalArgumentException("Unknown Uri: " + uri);
		}
		if (getContext() != null) {
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		}
		return cursor;
	}

	@Nullable
	@Override
	public String getType(@NonNull Uri uri) {
		switch (uriMatcher.match(uri)){
			case LIST_ITEMS:
				return CONTENT_LIST;
			case PARTICULAR_ITEM:
				return CONTENT_ITEM;
			default:
				throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}


	@Override
	public int delete(@NonNull Uri uri, @Nullable String selection,
					  @Nullable String[] selectionArgs){
		int count;
		SQLiteDatabase database = databaseHelper.getWritableDatabase();
		count = database.delete(NoteTable.NAME, selection, selectionArgs);
		if (getContext() != null) {
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return count;
	}

	@Nullable
	@Override
	public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
		SQLiteDatabase database = databaseHelper.getWritableDatabase();
		if (uriMatcher.match(uri) != 1) {
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		long rowId = database.insert(NoteTable.NAME, null, values);
		if (rowId > 0) {
			Uri articleUri = ContentUris.withAppendedId(CONTENT_URI_ITEMS, rowId);
			if (getContext() != null) {
				getContext().getContentResolver().notifyChange(articleUri, null);
			}
			return articleUri;
		}
		throw new IllegalArgumentException("Unknown URI: " + uri);
	}


	@Override
	public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
		SQLiteDatabase database = databaseHelper.getWritableDatabase();
		int numIserted = 0;
		switch (uriMatcher.match(uri)){
			case LIST_ITEMS:
				numIserted = insertDifTables(database, NoteTable.NAME, uri, values);
				break;
			case LIST_FAVORITES:
				numIserted = insertDifTables(database, FavoriteTable.NAME, uri, values);
				break;
			case LIST_RATING:
				numIserted = insertDifTables(database, RatingTable.NAME, uri, values);
				break;
			default:
				break;
		}

		return numIserted;
	}


	private int insertDifTables(SQLiteDatabase database, String tableName, Uri uri, ContentValues[] values){
		int numIserted = 0;
		database.delete(tableName, null, null);
		try {
			for (ContentValues cv : values){
				long id = database.insertOrThrow(tableName, null, cv);
				if (id <= 0){
					throw new SQLException("Failed to insert row into " + uri);
				}
			}
			database.setTransactionSuccessful();
			if (getContext() != null) {
				getContext().getContentResolver().notifyChange(uri, null);
			}
			numIserted = values.length;
		} finally {
			database.endTransaction();
		}
		return numIserted;
	}


	@Override
	public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
					  @Nullable String[] selectionArgs) {
		SQLiteDatabase db = databaseHelper.getWritableDatabase();
		int count = 0;
		switch (uriMatcher.match(uri)){
			case LIST_ITEMS:
				count = db.update(NoteTable.NAME, values, selection, selectionArgs);
				break;

			case PARTICULAR_ITEM:
				String rowId = uri.getPathSegments().get(1);
				count = db.update(NoteTable.NAME, values, NoteTable.Cols.UUID + " = " + rowId +
						(! TextUtils.isEmpty(selection) ? " AND (" + ")" : ""), selectionArgs);
				break;
			default:
				throw new IllegalArgumentException("Unknown Uri: " + uri );
		}
		if (getContext() != null){
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return count;
	}
}
