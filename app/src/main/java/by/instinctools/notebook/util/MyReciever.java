package by.instinctools.notebook.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;

import by.instinctools.notebook.App;

/**
 * Created by Nerallan on 1/30/2019.
 */

public class MyReciever extends BroadcastReceiver {

	private static final String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";

	public void onReceive(@NonNull Context context, @NonNull Intent intent) {
		if (intent.getAction() != null && intent.getAction().equals(BOOT_COMPLETED)){
			startAlarm();
		}
	}

	private void startAlarm(){
		Intent intent = new Intent(App.getInstance(), DownloadIntentService.class);
		PendingIntent pendingIntent = PendingIntent.getService(App.getInstance(), 0, intent, 0);
		AlarmManager manager = (AlarmManager) App.getInstance().getSystemService(Context.ALARM_SERVICE);
		int interval = 60 * 1000;
		if (manager != null) {
			manager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), interval, pendingIntent);
		}
		Log.d("ALARM", "alarm is active!!");
	}
}