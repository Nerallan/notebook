package by.instinctools.notebook.util;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

import by.instinctools.notebook.App;
import by.instinctools.notebook.Parser;
import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.model.Note;
import by.instinctools.notebook.model.Response;

import static by.instinctools.notebook.database.NoteDbSchema.FavoriteTable;
import static by.instinctools.notebook.database.NoteDbSchema.NoteTable;
import static by.instinctools.notebook.database.NoteDbSchema.RatingTable;
import static by.instinctools.notebook.util.NoteProvider.CONTENT_URI_FAVORITES;
import static by.instinctools.notebook.util.NoteProvider.CONTENT_URI_ITEMS;
import static by.instinctools.notebook.util.NoteProvider.CONTENT_URI_RATING;


/**
 * Created by Nerallan on 1/28/2019.
 */

public class DownloadIntentService extends IntentService {

	private static final String TAG = "DownloadIntentService";
	static final String JSON_URL_ITEMS = "https://notebook-cbc46.firebaseio.com/data/items/.json?auth=";
	static final String JSON_URL_FAVORITES = "https://notebook-cbc46.firebaseio.com/data/favorites/.json?auth=";
	static final String JSON_URL_RATING = "https://notebook-cbc46.firebaseio.com//data/rating/.json?auth=";
	private final String API_KEY = "AIzaSyDU7fEkDScwg9GpPtWAtMznJJ_kvfcCvCc";
	private final String tokenUrl = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + API_KEY;
	private final String typeItems = "items";
	private final String typeFavorites = "favorites";
	private final String typeRating = "rating";
	static final int STATUS_CODE_OK = 200;
	Parser parser;

	private int status = 0;

	public DownloadIntentService() {
		super("DownloadIntentService");
	}


	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		Log.d(TAG, "Service Started!");

		parser = new Parser();
		getDataByUrl(JSON_URL_ITEMS, typeItems);
		getDataByUrl(JSON_URL_FAVORITES, typeFavorites);
		getDataByUrl(JSON_URL_RATING, typeRating);
	}

	private void getDataByUrl(String urlType, String type){
		String urlPath = urlType + PrefManager.getInstance(
				App.getInstance()).getIdToken();
		HttpURLConnection urlConnection;
		List<Note> noteList = null;
		try {
			URL url = new URL(urlPath);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			int statusCode = urlConnection.getResponseCode();
			if (statusCode == STATUS_CODE_OK) {
				Log.d("DOWNLOAD", "NOTES IS DOWNLOADING");
				noteList = parser.readData(urlConnection.getInputStream(), type);
				if (noteList != null) {
					ContentValues[] values = createBulkInsertValues(noteList, type);
					switch (type) {
						case typeItems:
							getContentResolver().bulkInsert(CONTENT_URI_ITEMS, values);
							break;
						case typeFavorites:
							getContentResolver().bulkInsert(CONTENT_URI_FAVORITES, values);
							break;
						case typeRating:
							getContentResolver().bulkInsert(CONTENT_URI_RATING, values);
							break;
						default:
							break;
					}

				}
			}
		} catch (Exception e) {
			Log.d(TAG, e.getLocalizedMessage());
		}
	}


	private ContentValues[] createBulkInsertValues(List<Note> notes, String type){
		ContentValues[] contentValues = new ContentValues[notes.size()];

		int favoriteCounter = 0;
		for (int index = 0; index < notes.size(); index++){
			ContentValues noteValue = new ContentValues();
			switch (type) {
				case typeItems:
					noteValue.put(NoteTable.Cols.UUID,  index + 1);
					noteValue.put(NoteTable.Cols.TITLE, notes.get(index).getTitle());
					noteValue.put(NoteTable.Cols.DESCRIPTION, notes.get(index).getDescription());
					noteValue.put(NoteTable.Cols.IMAGE_URL, notes.get(index).getImagePath());
					contentValues[index] = noteValue;
					break;
				case typeFavorites:
					if(notes.get(index).isFavorite()){
						noteValue.put(FavoriteTable.Cols.UUID, favoriteCounter + 1);
						noteValue.put(FavoriteTable.Cols.ITEM_ID, index);
						noteValue.put(FavoriteTable.Cols.TIMESTAMP, notes.get(index).getTimestamp());
						contentValues[favoriteCounter] = noteValue;
						favoriteCounter++;
					}
					break;
				case typeRating:
					noteValue.put(RatingTable.Cols.UUID, index + 1);
					noteValue.put(RatingTable.Cols.ITEM_ID, index);
					noteValue.put(RatingTable.Cols.RATE_VALUE, notes.get(index).getRating());
					contentValues[index] = noteValue;
					break;
				default:
					break;
			}
		}
		return contentValues;
	}


	private Response updateToken() {
		Response response = null;
		String refreshToken = PrefManager.getInstance(App.getInstance()).getRefreshToken();

		JSONObject jsonObject = new JSONObject();
		HttpURLConnection connection = null;
		try {
			jsonObject.put("grant_type", "refresh_token");
			jsonObject.put("refresh_token", refreshToken);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			connection = (HttpURLConnection) new URL(tokenUrl).openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				connection.setRequestMethod("POST");
				connection.setRequestProperty("charset", "utf-8");
			}
		} catch (ProtocolException e) {
			e.printStackTrace();
		}

		InputStream in = IOOperations(connection, jsonObject);

		Parser parser = new Parser();
		try {
			if (status == STATUS_CODE_OK) {
				response = parser.readResponse(in);
			} else {
				response = parser.readError(in);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}


	private InputStream IOOperations(HttpURLConnection connection, JSONObject jsonObject) {
		OutputStream out;
		InputStream in = null;
		try {
			if (connection != null) {
				out = connection.getOutputStream();
				out.write(jsonObject.toString().getBytes("UTF-8"));
				out.flush();

				status = connection.getResponseCode();
				if (status == STATUS_CODE_OK) {
					in = connection.getInputStream();
				} else {
					in = connection.getErrorStream();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return in;
	}

}
