package by.instinctools.notebook.util;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import by.instinctools.notebook.App;
import by.instinctools.notebook.Parser;
import by.instinctools.notebook.PrefManager;

public class FavoriteIntentService extends IntentService {

	private static final String TAG = "FavoriteIntentService";
	static final String JSON_URL_FAVORITES = "https://notebook-cbc46.firebaseio.com/data/favorites/";
	private final String API_KEY = "AIzaSyDU7fEkDScwg9GpPtWAtMznJJ_kvfcCvCc";
	private final String tokenUrl = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + API_KEY;
	private final String typeFavorites = "favorites";
	private final String PUT = "put";
	private final String DELETE = "delete";
	static final int STATUS_CODE_OK = 200;
	Parser parser;


	public FavoriteIntentService() {
		super("FavoriteIntentService");
	}


	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		int id = 0;
		long timestamp = 0;
		String operationType = null;
		if (intent != null) {
			id = intent.getIntExtra("position", 0);
			timestamp = intent.getLongExtra("timestamp", 0);
			operationType = intent.getStringExtra("operationType");
		}
		String urlPath = JSON_URL_FAVORITES + PrefManager.getInstance(App.getInstance())
														 .getUserId() + "/" + id + ".json?auth=" + PrefManager
				.getInstance(App.getInstance()).getIdToken();
		int status = 0;
		if (operationType != null) {
			switch (operationType) {
				case PUT:
					putData(urlPath, timestamp);
					break;
				case DELETE:
					deleteData(urlPath);
					break;
				default:
					break;
			}
		}

	}


	private void putData(String urlPath, Long timestamp){
		URL url = null;
		HttpURLConnection httpCon = null;
		InputStream inputStream = null;
		int status = 0;
		try {
			url = new URL(urlPath);
			httpCon = (HttpURLConnection) url.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestMethod("PUT");
			httpCon.addRequestProperty("Content-Type", "application/json");
			OutputStream out = httpCon.getOutputStream();
			JSONObject jsonObject = makeFavoriteObject(timestamp);
			out.write(jsonObject.toString().getBytes("UTF-8"));
			out.flush();
			status = httpCon.getResponseCode();
			if (status == STATUS_CODE_OK) {
				inputStream = httpCon.getInputStream();
			} else {
				inputStream = httpCon.getErrorStream();
			}
			inputStream.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private void deleteData(String urlPath){
		URL url = null;
		HttpURLConnection httpCon = null;
		InputStream inputStream = null;
		int status = 0;
		try {
			url = new URL(urlPath);
			httpCon = (HttpURLConnection) url.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestMethod("DELETE");
			httpCon.addRequestProperty("Content-Type", "application/json");
			OutputStream out = httpCon.getOutputStream();
			out.flush();
			status = httpCon.getResponseCode();
			if (status == STATUS_CODE_OK) {
				inputStream = httpCon.getInputStream();
			} else {
				inputStream = httpCon.getErrorStream();
			}
			inputStream.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private JSONObject makeFavoriteObject(Long timestamp) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("timestamp", timestamp);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
}


