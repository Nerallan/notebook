package by.instinctools.notebook.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import by.instinctools.notebook.App;
import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.R;

/**
 * Created by Nerallan on 1/14/2019.
 */

public class AsyncTaskLoadImage extends AsyncTask<String, Void, Bitmap> {

	private static final int STATUS_CODE_OK = 200;

	@Nullable
	private final WeakReference<ImageView> fruitImageView;
	@Nullable
	private String imagePath;
	private static Boolean big;

	AsyncTaskLoadImage(@Nullable ImageView imageView, Boolean big) {
		this.fruitImageView = new WeakReference<>(imageView);
		AsyncTaskLoadImage.big = big;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		ImageView imageView = null;
		if (fruitImageView != null) {
			imageView = fruitImageView.get();
			if (imageView != null){
				if (!big) {
					Drawable placeholder =
							imageView.getContext().getResources().getDrawable(R.drawable.placeholder);
					imageView.setImageDrawable(placeholder);
				}
			}
		}
	}


	@Nullable
	@Override
	protected Bitmap doInBackground(@NonNull String... params) {
		imagePath = params[0];
		ImageLoadManager imageLoadManager = ImageLoadManager.getInstance();
		Bitmap bitmap = null;
		if (imagePath != null) {
			bitmap = imageLoadManager.getFromExternalStorage(imagePath, big);
		}
		ImageView imageView = null;
		if (fruitImageView != null){
			imageView = fruitImageView.get();
		}
		if (bitmap == null && imageView != null) {
			bitmap = downloadBitmap(imagePath);
			if (imageLoadManager.checkUrl(imageView, imagePath)){
				imageLoadManager.addBitmapToMemoryCache(imagePath, bitmap, big);
				if (PrefManager.getInstance(App.getInstance()).getMemorySettings()) {
					imageLoadManager.saveTempBitmap(bitmap, imagePath, big);
				}
			}
		}
		return bitmap;
	}


	@Override
	protected void onPostExecute(@Nullable Bitmap bitmap) {
		if (isCancelled()) {
			bitmap = null;
		}
		if (fruitImageView != null) {
			ImageView imageView = fruitImageView.get();
			ImageLoadManager imageLoadManager = ImageLoadManager.getInstance();
			if (bitmap != null && imageView != null && imageLoadManager.checkUrl(imageView, imagePath)) {
				imageView.setImageBitmap(bitmap);
			}
		}
	}

	@Nullable
	private Bitmap downloadBitmap(String url) {
		HttpURLConnection urlConnection = null;
		try {
			URL uri = new URL(url);
			urlConnection = (HttpURLConnection) uri.openConnection();
			int statusCode = urlConnection.getResponseCode();
			if (statusCode != STATUS_CODE_OK) {
				return null;
			}
			return decodeBitmapFromUri(uri);
		} catch (Exception e) {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
			Log.w("ImageDownloader", "Error downloading image from " + url);
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return null;
	}

	private static int calculateInSampleSize(BitmapFactory.Options options) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int scaleFactor = 1;

		int TARGET_IMAGE_DIMENSION;
		if (big) {
			TARGET_IMAGE_DIMENSION = (App.getInstance().getResources()
										 .getDimensionPixelOffset(R.dimen.big_image_dimension));
		} else {
			TARGET_IMAGE_DIMENSION = (App.getInstance().getResources()
										 .getDimensionPixelOffset(R.dimen.small_image_dimension));
		}
		if (height > TARGET_IMAGE_DIMENSION || width > TARGET_IMAGE_DIMENSION) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			while ((halfHeight / scaleFactor) > TARGET_IMAGE_DIMENSION && (halfWidth / scaleFactor) > TARGET_IMAGE_DIMENSION) {
				scaleFactor *= 2;
			}
		}
		return scaleFactor;
	}

	@Nullable
	private static Bitmap decodeBitmapFromUri(@Nullable URL uri) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		if (uri != null) {
			try {
				BitmapFactory.decodeStream(uri.openConnection().getInputStream(), null, options);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		options.inSampleSize = calculateInSampleSize(options);
		options.inJustDecodeBounds = false;
		Bitmap bitmapFactory = null;
		if (uri != null) {
			try {
				bitmapFactory = BitmapFactory
						.decodeStream(uri.openConnection().getInputStream(), null, options);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return bitmapFactory;
	}
}