package by.instinctools.notebook.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import by.instinctools.notebook.App;
import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.model.User;

public class SharedPrefLoader extends BaseLoader<User> {

	public SharedPrefLoader(@NonNull Context context) {
		super(context);
	}

	@Nullable
	@Override
	public User loadInBackground() {
		return PrefManager.getInstance(App.getInstance()).getUserData();
	}
}
