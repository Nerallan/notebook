package by.instinctools.notebook.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.WeakHashMap;

import by.instinctools.notebook.App;

import static android.os.Environment.MEDIA_MOUNTED;

/**
 * Created by Nerallan on 1/20/2019.
 */

public class ImageLoadManager {

	private static final String TAG = "ImageLoadManager";

	@Nullable
	private static ImageLoadManager loadManager = null;
	@Nullable
	private static LruCache<String, Bitmap> lruCache;
	@NonNull
	private Map<ImageView, String> checkLinkMap = new WeakHashMap<>();


	@NonNull
	public static ImageLoadManager getInstance() {
		if (loadManager == null) {
			loadManager = new ImageLoadManager();
		}
		return loadManager;
	}


	private ImageLoadManager() {
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
		final int cacheSize = maxMemory / 8;

		lruCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				return bitmap.getByteCount();
			}
		};
	}


	public void load(@NonNull String url, @NonNull ImageView imageView, boolean big) {
		checkLinkMap.put(imageView, url);
		Bitmap bitmap = getBitmapFromMemoryCache(url, big);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			AsyncTaskLoadImage asyncTaskLoadImage = new AsyncTaskLoadImage(imageView, big);
			asyncTaskLoadImage.execute(url);
		}
	}


	private Bitmap getBitmapFromMemoryCache(@Nullable String key, @NonNull Boolean big) {
		if (lruCache != null && key != null) {
			String code = String.valueOf(key.hashCode());
			code += big ? "b" : "s";
			return lruCache.get(code);
		}
		return null;
	}


	void addBitmapToMemoryCache(@Nullable String key, @Nullable Bitmap bitmap, @NonNull Boolean big) {
		if (lruCache != null && bitmap != null && key != null) {
			String code = String.valueOf(key.hashCode());
			code += big ? "b" : "s";
			lruCache.put(code, bitmap);
		}
	}

	boolean checkUrl(ImageView imageView, String url) {
		return url.equals(checkLinkMap.get(imageView));
	}


	void saveTempBitmap(@Nullable Bitmap bitmap, @Nullable String imagePath, @NonNull Boolean big) {
		if (isExternalStorageWritable()) {
			if (bitmap != null && imagePath != null) {
				addToExternalStorage(bitmap, imagePath, big);
			}
		} else {
			Log.d(TAG, "can't read / write external storage");
		}
	}

	private boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		return MEDIA_MOUNTED.equals(state);
	}


	private void addToExternalStorage(@NonNull Bitmap bitmap, @NonNull String imagePath,
									  @NonNull Boolean big) {
		File fileDir = App.getInstance().getExternalCacheDir();
		String path = null;
		if (fileDir != null) {
			path = fileDir.getAbsolutePath();
		}
		OutputStream outputStream = null;
		File myDir = new File(path, "/saved_images");
		if (!myDir.exists()) {
			myDir.mkdir();
		}
		String fileName;
		if (big) {
			fileName = String.valueOf(imagePath.hashCode() + "b");
		} else {
			fileName = String.valueOf(imagePath.hashCode() + "s");
		}
		File file = new File(myDir, fileName);
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			outputStream = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 70, outputStream);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (outputStream != null) {
					outputStream.flush();
					outputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Nullable
	Bitmap getFromExternalStorage(@NonNull String imagePath, @NonNull Boolean big) {
		File fileDir = App.getInstance().getExternalCacheDir();
		String path = null;
		if (fileDir != null) {
			path = fileDir.getAbsolutePath();
		}
		String fileName;
		if (big) {
			fileName = String.valueOf(imagePath.hashCode() + "b");
		} else {
			fileName = String.valueOf(imagePath.hashCode() + "s");
		}
		File myDir = new File(path, "/saved_images");
		File file = new File(myDir, fileName);
		if (file.exists()) {
			return BitmapFactory.decodeFile(String.valueOf(file));
		}
		return null;
	}
}
