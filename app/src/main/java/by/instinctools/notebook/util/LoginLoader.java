package by.instinctools.notebook.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import by.instinctools.notebook.App;
import by.instinctools.notebook.Parser;
import by.instinctools.notebook.PrefManager;
import by.instinctools.notebook.model.Response;
import by.instinctools.notebook.model.User;

public class LoginLoader extends BaseLoader<Response> {

	private static final String TAG = "LoginLoader";
	private final String API_KEY = "AIzaSyDU7fEkDScwg9GpPtWAtMznJJ_kvfcCvCc";
	private String tokenUrl = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + API_KEY;
	private static final String JSON_URL = "https://notebook-cbc46.firebaseio.com/user/";
	private static final int STATUS_CODE_OK = 200;

	@Nullable
	private String userCreds;
	private int status = 0;

	public LoginLoader(@NonNull Context context, @Nullable String userCreds) {
		super(context);
		this.userCreds = userCreds;
	}

	@Nullable
	@Override
	public Response loadInBackground() {
		User user = null;
		Response response = downloadToken();
		PrefManager manager = PrefManager.getInstance(App.getInstance());
		manager.saveResponseInPref(response);
		String error = manager.getErrorMessage();
		String urlPath = JSON_URL + manager.getUserId() + ".json?auth=" + manager.getIdToken();
		if (manager.getIdToken() != null && error == null){
			HttpURLConnection urlConnection;
			try {
				URL url = new URL(urlPath);
				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setRequestMethod("GET");
				int statusCode = urlConnection.getResponseCode();
				if (statusCode == STATUS_CODE_OK) {
					Parser parser = new Parser();
					user = parser.readCurrentUser(urlConnection.getInputStream());
					if (user != null) {
						PrefManager.getInstance(App.getInstance())
								   .saveUserData(user.getUsername(), user.getEmail(),
										   user.getUserImage());
					}
				}
			} catch (Exception e) {
				Log.d(TAG, e.getLocalizedMessage());
			}
		}
		return response;
	}

//	private Response updateToken() {
//		Response response = null;
//		String refreshToken = PrefManager.getInstance(App.getInstance()).getRefreshToken();
//
//		JSONObject jsonObject = new JSONObject();
//		HttpURLConnection connection = null;
//		try {
//			jsonObject.put("grant_type", "refresh_token");
//			jsonObject.put("refresh_token", refreshToken);
//		} catch (JSONException e) {
//			e.printStackTrace();
//		}
//
//		try {
//			connection = (HttpURLConnection) new URL(tokenUrl).openConnection();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		try {
//			if (connection != null) {
//				connection.setDoInput(true);
//				connection.setDoOutput(true);
//				connection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//				connection.setRequestMethod("POST");
//				connection.setRequestProperty("charset", "utf-8");
//			}
//		} catch (ProtocolException e) {
//			e.printStackTrace();
//		}
//
//		InputStream in = IOOperations(connection, jsonObject);
//
//		Parser parser = new Parser();
//		try {
//			if (status == STATUS_CODE_OK) {
//				response = parser.readResponse(in);
//			} else {
//				response = parser.readError(in);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return response;
//	}


	private Response downloadToken() {
		Response response = null;
		String userCredentials = userCreds;
		String[] credential = new String[0];
		String basicAuth = null;
		if (userCredentials != null) {
			credential = userCredentials.split(":");
			basicAuth = "Basic " + Base64
					.encodeToString(userCredentials.getBytes(), Base64.NO_WRAP);
		}

		JSONObject jsonObject = new JSONObject();
		String email = credential[0];
		String password = credential[1];
		HttpURLConnection connection = null;
		try {
			jsonObject.put("email", email);
			jsonObject.put("password", password);
			jsonObject.put("returnSecureToken", true);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		try {
			connection = (HttpURLConnection) new URL(tokenUrl).openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (connection != null) {
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.addRequestProperty("Authorization", basicAuth);
				connection.addRequestProperty("Content-Type", "application/json");
				connection.setRequestMethod("POST");
				connection.setRequestProperty("charset", "utf-8");
				Log.d(TAG, "getAccessToken: " + connection.toString());
			}
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
		InputStream in = IOOperations(connection, jsonObject);

		Parser parser = new Parser();
		try {
			if (status == STATUS_CODE_OK) {
				response = parser.readResponse(in);
			} else {
				response = parser.readError(in);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}


	private InputStream IOOperations(HttpURLConnection connection, JSONObject jsonObject) {
		OutputStream out;
		InputStream in = null;
		try {
			if (connection != null) {
				out = connection.getOutputStream();
				out.write(jsonObject.toString().getBytes("UTF-8"));
				out.flush();

				status = connection.getResponseCode();
				if (status == STATUS_CODE_OK) {
					in = connection.getInputStream();
				} else {
					in = connection.getErrorStream();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return in;
	}
}