package by.instinctools.notebook.util;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

public abstract class BaseLoader<T> extends AsyncTaskLoader<T> {

	private static final String TAG = "BaseLoader";

	@Nullable
	private T model;
	private final InterestingConfigChanges lastConfig = new InterestingConfigChanges();

	public BaseLoader(@NonNull Context context) {
		super(context);
	}


	@Override
	public void deliverResult(@Nullable T data) {
		if (isStarted()) {
			super.deliverResult(data);
		}
		Log.d(TAG, "deliverResult");
	}


	public static class InterestingConfigChanges {
		final Configuration mLastConfiguration = new Configuration();
		int mLastDensity;

		boolean applyNewConfig(Resources res) {
			int configChanges = mLastConfiguration.updateFrom(res.getConfiguration());
			boolean densityChanged = mLastDensity != res.getDisplayMetrics().densityDpi;
			if (densityChanged || (configChanges&(ActivityInfo.CONFIG_LOCALE
					|ActivityInfo.CONFIG_UI_MODE|ActivityInfo.CONFIG_SCREEN_LAYOUT)) != 0) {
				mLastDensity = res.getDisplayMetrics().densityDpi;
				return true;
			}
			return false;
		}

	}

	@Override
	protected void onStartLoading() {
		if (model != null) {
			deliverResult(model);
		}
		boolean configChange = lastConfig.applyNewConfig(getContext().getResources());

		if (takeContentChanged() || model == null || configChange) {
			forceLoad();
		}
		Log.d(TAG, "onStartLoading");
	}

	@Override
	protected void onStopLoading() {
		super.onStopLoading();
		cancelLoad();
		Log.d(TAG, "onStopLoading");
	}

	@Override
	public void onCanceled(@Nullable T data) {
		super.onCanceled(data);
		Log.d(TAG, "onCanceled");
	}

	@Override
	protected void onForceLoad() {
		super.onForceLoad();
		Log.d(TAG, "onForceLoad");
	}

	@Override
	protected void onAbandon() {
		super.onAbandon();
		Log.d(TAG, "onAbandon");
	}

	@Override
	protected void onReset() {
		super.onReset();
		Log.d(TAG, "onReset");
	}
}
