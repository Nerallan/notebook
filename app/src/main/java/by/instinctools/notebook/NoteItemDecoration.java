package by.instinctools.notebook;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by Nerallan on 1/7/2019.
 */

public class NoteItemDecoration extends RecyclerView.ItemDecoration {

	private static final String TAG = "ItemDecorator";

	@Nullable
	private final Paint paint;


	public NoteItemDecoration(@NonNull Context context, @ColorInt int color,
							  @FloatRange(from = 0, fromInclusive = false) float heightDp) {
		paint = new Paint();
		paint.setColor(color);
		final float thickness = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, heightDp,
														  context.getResources()
																 .getDisplayMetrics());
		paint.setStrokeWidth(thickness);
	}


	@Override
	public void onDraw(@NonNull Canvas pCanvas, @NonNull RecyclerView parent,
					   @NonNull RecyclerView.State state) {
		int offset = 0;
		if (paint != null) {
			offset = (int) (paint.getStrokeWidth() / 2);
		}

		for (int i = 0; i < parent.getChildCount(); i++) {
			final View view = parent.getChildAt(i);
			final RecyclerView.LayoutParams params =
					(RecyclerView.LayoutParams) view.getLayoutParams();

			int count = parent.getChildCount();
			View view1 = parent.getChildAt(0);
			View view2 = parent.getChildAt(count - 1);

			if (view1 != null && view2 != null) {
				final int position = params.getViewAdapterPosition();

				RecyclerView.ViewHolder viewHolder1 = parent.findContainingViewHolder(view1);
				RecyclerView.ViewHolder viewHolder2 = parent.findContainingViewHolder(view2);

				if (viewHolder1 != null && viewHolder2 != null) {
					if (position == 0 && viewHolder1.getLayoutPosition() == 0) {
						Log.d(TAG, "1   " + position + "  " + viewHolder1.getLayoutPosition());
					} else if (position == viewHolder2.getLayoutPosition()) {
						Log.d(TAG, "2   " + position + "  " + viewHolder2.getLayoutPosition());
					} else {
						System.out.println("3   " + position + "  ");
						if (paint != null && position < state.getItemCount()) {
							pCanvas.drawLine(view.getLeft(), view.getBottom() + offset,
											 view.getRight(), view.getBottom() + offset, paint);
						}
					}
				}
			}
		}
	}
}
